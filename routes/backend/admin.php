<?php
Route::get('/dashboard', 'DashboardController@index')->name('admin.dashboard');

//Profile route
Route::group(['namespace' => 'Article','as' => 'admin.'], function () {
  Route::resource('articles', 'ArticleController', ['names' => [
      'index'     => 'article.index',
      'create'    => 'article.create',
      'store'     => 'article.store',
      'edit'      => 'article.edit',
      'update'    => 'article.update',
      'show'      => 'article.show',
      'destroy'   => 'article.destroy'
  ]]);

});
