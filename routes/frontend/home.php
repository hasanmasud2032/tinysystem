  <?php

  Route::get('/', 'HomeController@index')->name('home');

  Route::get('/post-details/{id}', 'ArticleController@showPost')->name('post.show');

  Route::get('/video-details/{id}', 'ArticleController@showVideo')->name('video.show');
