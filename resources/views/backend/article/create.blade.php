@extends('backend.layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row col-sm">
                <h5><i class="fa fa-plus pr-2"></i>Add Article</h5>
            </div>
        </div>
        {!! Form::open(['route'=>'backend.admin.article.store', 'method'=>'post','enctype'=>'multipart/form-data','id'=>'dataForm']) !!}
        <div class="card-body">
            <div class="row">
              <div class="col-md-12 form-group">
                  {{ Form::label('title','Title : ',['class'=>'font-weight-bold required-star']) }}
                  {!! Form::text('title','',['class'=>$errors->has('title')?'form-control is-invalid':'form-control required','placeholder'=>'Title']) !!}
              </div>
              <div class="col-md-12 form-group">
                  {!! Form::label('description','Description : ',['class'=>'font-weight-bold required-star']) !!}
                  {!! Form::textarea('description','',['class'=>$errors->has('description')?'form-control is-invalid':'form-control required','rows'=>'5','placeholder'=>'Description','id'=>'description']) !!}
              </div>
              <div class="col-md-6 form-group">
                  {!! Form::label('article_type_id','Article Type : ',['class'=>'font-weight-bold required-star']) !!}
                  {!! Form::select('article_type_id',['video'=>'Video','post'=>'Post'],'',['class'=>$errors->has('article_type_id')?'form-control is-invalid':'form-control required','placeholder'=>'Select article type','id'=>'article-type']) !!}
              </div>
              <div class="col-md-6 form-group">
                  {!! Form::label('section_id','Post Appear Section Name : ',['class'=>'font-weight-bold required-star']) !!}
                  {!! Form::select('section_id',['1'=>'Section One','2'=>'Section Two'],'',['class'=>$errors->has('section_id')?'form-control is-invalid':'form-control required','placeholder'=>'Select section']) !!}
              </div>
              <div class="col-md-6 form-group" id="link-div">
                  {{ Form::label('link','Video link : ',['class'=>'font-weight-bold required-star']) }}
                  {!! Form::text('link','',['class'=>$errors->has('link')?'form-control is-invalid':'form-control required','placeholder'=>'Video link here']) !!}
              </div>
              <div class="col-md-6 form-group">
                  {!! Form::label('status','Status : ',['class'=>'font-weight-bold required-star']) !!}
                  {!! Form::select('status',[1=>'Active',0=>'Inactive'],'',['class'=>$errors->has('status')?'form-control is-invalid':'form-control required']) !!}
              </div>
              <div class=" col-md-6 form-group" id="image-div">
                  {{ Form::label('image', 'Image:',['class'=>'font-weight-bold required-star']) }}
                  <br/>
                  <img class="img img-responsive img-thumbnail" src="{{ url('/uploads/img/photo.png') }}"
                       id="firstImageViewer" style="height: 100px; width: 120px;">
                  <br>
                  <label class="btn btn-secondary btn-sm rounded-0" style="width: 120px;">
                    <input required onchange="changeFirstImage(this)" type="file" name="image" style="visibility:hidden;position:absolute">
                      <i class="fa fa-image mr-1 text-light"></i>Browse
                  </label>
                  <br>
                  <span>Max height : 400px max width : 400px</span>
                  <br>
                  <span id="first_photo_error" class="text-danger font-weight-bold mb-2"
                        style="font-size: 16px;"></span>
              </div>
            </div>
        </div>
        <div class="card-footer">
            <a href="{{ route('backend.admin.article.index') }}" class="btn btn-primary"><i class="fa fa-backward pr-2"></i>Back</a>
            <button type="submit" class="btn float-right btn-primary"><i class="fa fa-save pr-2"></i>Save</button>
        </div>
        {!! Form::close() !!}
    </div><!--card-->
@endsection

@section('footer-script')
    <script src="{{ asset('assets/backend/dist/js/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            /**********************
             VALIDATION START HERE
             **********************/
            $('#dataForm').validate({
                errorPlacement: function () {
                    return false;
                }
            });

            var value = $('#article-type').find('option:selected').val();
            if(value == 'video')
            {
              $('#link-div').show();
              $('#image-div').show();
            }
            else if(value == 'post')
            {
                $('#image-div').show();
                $('#link-div').hide();
            }
            else
            {
              $('#image-div').hide();
              $('#link-div').hide();
            }
            $('#article-type').change(function(){
              var value =$('#article-type').val();
              if(value == 'video'){
              $('#image-div').show();
              $('#link-div').show();
              }
              else if(value == 'post')
              {
                $('#image-div').show();
                $('#link-div').hide();
              }
              else {
                $('#image-div').hide();
                $('#link-div').hide();
              }
            });
        });

        /*************************
         PHOTO PREVIEW START HERE
         ************************/
        function changeFirstImage(input) {
            if (input.files && input.files[0]) {
                $("#first_photo_error").html('');
                var mime_type = input.files[0].type;
                if (!(mime_type == 'image/jpeg' || mime_type == 'image/svg+xml' || mime_type == 'image/jpg' || mime_type == 'image/png')) {
                    $("#first_photo_error").html("Image format is not valid. Only PNG or JPEG or JPG type images are allowed.");
                    return false;
                }
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#firstImageViewer').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        /*********************
            RICH TEXT HERE
         *********************/
        tinymce.init({
            selector: '#description',
            height: 200,
            content_style: 'img {max-width: 100%;}',
            max_chars: 1000, // max. allowed chars
            setup: function (editor) {
                editor.on('init change', function () {
                    editor.save();
                });
            },

            plugins : 'textcolor advlist lists link autolink autosave code preview searchreplace wordcount media table emoticons image imagetools',
            menubar:'file edit format',
            toolbar:'bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent|link image media | forecolor backcolor emoticons | table preview',

            image_title: true,
            automatic_uploads: true,
            file_picker_types: 'image',
            file_picker_callback: function(cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');
                input.onchange = function() {
                    var file = this.files[0];
                    if(Math.round((this.files[0].size/1024)*100/100) > 1000){
                        alert('Image size maximum 200KB');
                        return false
                    }else {
                        var reader = new FileReader();
                        reader.readAsDataURL(file);
                        reader.onload = function () {
                            var id = 'blobid' + (new Date()).getTime();
                            var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                            var base64 = reader.result.split(',')[1];
                            var blobInfo = blobCache.create(id, file, base64);
                            blobCache.add(blobInfo);
                            cb(blobInfo.blobUri(), { title: file.name });
                        };
                    }
                };
                input.click();
            }
        });
    </script>
@endsection
