@extends('backend.layouts.app')
@section('content')
    <ol class="breadcrumb alert alert-primary p-2">
        <li class="breadcrumb-item"><strong>Creator Name :  </strong><span>{{ $article->author_name }}</span></li>
    </ol>
    <div class="card">
        <div class="card-header">
            <div class="row col-sm">
                <h5><i class="fa fa-list-alt pr-2"></i>Article Details</h5>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12 form-group">
                    <div class="row">
                        <div class="col-lg-4">
                            Status
                        </div>
                        <div class="col-lg-8">
                            @if($article->status == 1)
                                <label class='badge badge-success'>Active</label>
                            @else
                                <label class='badge badge-danger'>Inactive</label>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-12 form-group">
                    <div class="row">
                        <div class="col-lg-4">
                            Title
                        </div>
                        <div class="col-lg-8">
                            {{$article->title}}
                        </div>
                    </div>
                </div>
                <div class="col-md-12 form-group">
                    <div class="row">
                        <div class="col-lg-4">
                            Article Type
                        </div>
                        <div class="col-lg-8">
                            {{$article->article_type_id }}
                        </div>
                    </div>
                </div>
                <div class="col-md-12 form-group">
                    <div class="row">
                        <div class="col-lg-4">
                          Section Name
                        </div>
                        <div class="col-lg-8">
                            {{$article->section_id == 1 ? 'Section One' : 'Section Two' }}
                        </div>
                    </div>
                </div>
                <div class="col-md-12 form-group">
                    <div class="row">
                        <div class="col-lg-4">
                          Image
                        </div>
                        <div class="col-lg-8">
                          <img src="{{asset('uploads/article/image/'.$article->image)}}" alt="image" width="50"
                               height="50">
                        </div>
                    </div>
                </div>
                @if($article->article_type_id == 'video')
                <div class="col-md-12 form-group">
                    <div class="row">
                        <div class="col-lg-4">
                         video Link
                        </div>
                        <div class="col-lg-8">
                          <iframe width="560" height="315" src="{{$article->video_link}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="row">
                <div class="col-md-12 form-group font-weight-bold">
                  <u>Description : </u>
                </div>
                <div class="col-md-12 form-group">
                      {!! $article->description !!}
                </div>
            </div>
            <div class="card-footer px-1">
                <a href="{{ route('backend.admin.article.index') }}" class="btn btn-primary"><i class="fa fa-backward pr-2"></i>
                    Back</a>
            </div>
        </div><!--card-->
@endsection
