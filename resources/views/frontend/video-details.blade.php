@extends('frontend.layouts.app')
@section('header-css')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<style media="screen">
  .company-image img{
    min-width: 100%;
    height: 420px;
  }
</style>
@endsection
@section('content')
<!--  ================= company details Start ================= -->
<div class="our-feautes-area less-border default-padding bg-theme-small">
    <div class="container">
        <div class="row">
            <div class="blog-items">
                <div class="blog-content col-md-12">
                  <div class="item" style="background:gray;">
                      <div class="content-box">
                        <h2 style="color:white">Detail page for Video</h2>
                      </div>
                  </div>
              </div>
            </div>
        </div>
      </div>
  </div>
@if($article)
<div class="about-area default-padding bg-theme-small">
    <div class="container">
        <div class="row text-center">
          <div class="col-md-12">
            <h2>{{$article->title}}</h2>
            <div class="thumb company-image">
              <iframe width="560" height="315" src="{{$article->video_link}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <p>{!! $article->description !!}</p>
          </div>
        </div>
    </div>
</div>
@endif
<div id="share" style="padding-bottom:30px"></div>
<!--  ================= Company details end ================= -->
@endsection

@section('footer-script')
<script>
  $("#share").jsSocials({
    showCount: true,
    showLabel: false,
    shares: [
     { share: "twitter", via: "artem_tabalin", hashtags: "search,google" },
     "facebook",
     "googleplus",
     "linkedin",
     "pinterest",
     "whatsapp"
    ]
  });
</script>
@endsection
