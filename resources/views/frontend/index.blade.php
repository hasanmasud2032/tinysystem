@extends('frontend.layouts.app')
@section('header-css')
<style media="screen">
body{
  line-height: 20px;
}
.clearfix::after {
  content: "";
  clear: both;
  display: table;
}

.custom-padding{
  padding: 10px;
}
.custom-padding p{
  margin-bottom: 0px;
  height: 30px;
  padding-top: 10px;
  line-height: 20px;
  overflow: hidden;
}

.custom-margin-bottom{
  margin-bottom: 0px;
}
.single-content{
  height: 200px;
  background: gray;
  position: relative;
}
.multi-content{
  height: 80px;
  background: gray;
  position: relative;
}

/* section two css start */
.section-two-single-content{
  height: 300px;
  position: relative;
}

.section-two-multi-content{
  height: 150px;
}
/* section two css end */

@media only screen and (max-width: 992px) {
  .multi-content{
      height: 250px;
      background:gray;
  }
  .single-content{
      height: 250px;
      background:gray;
  }
}

@media only screen and (max-width: 768px) {
  .multi-content{
      height: 250px;
      background:gray;
  }
  .single-content{
      height: 250px;
      background:gray;
  }
  .custom-width{
    width: 50%;
    float: left;
  }
}

@media only screen and (max-width: 575px) {
  .multi-content{
      height: 300px;
      background:gray;
  }
  .single-content{
      height: 300px;
      background:gray;
  }
  .custom-width{
    width: 100%;
  }
}

.btn-video{
  position: absolute;
  padding: 1px 6px 1px 6px;
  bottom: 0px;
  right: 0px;
  background: black;
  border-radius: 0px;
  color: white;
}

.btn-video:hover{
  color:white;
}

.multi-content span{
  overflow: hidden;
}

</style>
@endsection
@section('content')
<div class="our-feautes-area less-border default-padding bg-theme-small">
    <div class="container">
        <div class="row">
            <div class="blog-items">
                <div class="blog-content col-md-12">
                  <div class="item" style="background:gray;">
                      <div class="content-box">
                      </div>
                  </div>
              </div>
            </div>
        </div>
      </div>
  </div>
<div class="testimonials-area default-padding">
  <div class="container">
    <div class="row">
      @if(count($articleSectionOne)!=0)
      <div class="col-md-6 intro">
        <div class="row" style="padding-right:20px;">
          <div class="col-md-6 custom-padding">
            <div class="single-content">
              <a href="{{$articleSectionOne[0]->article_type_id == 'post' ?route('frontend.post.show',\App\Libraries\Encryption::encodeId($articleSectionOne[0]->id)) : route('frontend.video.show',\App\Libraries\Encryption::encodeId($articleSectionOne[0]->id))}}"><img width="100%" height="100%" src="{{$articleSectionOne[0]->image ? asset('uploads/article/image/'.$articleSectionOne[0]->image) : asset('assets/frontend/img/case/1.jpg')}}" alt=""></a>
              @if($articleSectionOne[0]->article_type_id == 'video')
              <a href="{{$articleSectionOne[0]->article_type_id == 'post' ?route('frontend.post.show',\App\Libraries\Encryption::encodeId($articleSectionOne[0]->id)) : route('frontend.video.show',\App\Libraries\Encryption::encodeId($articleSectionOne[0]->id))}}" class="btn btn-video"><i class="fas fa-play"></i></a>
              @endif
            </div>
            <p>{{$articleSectionOne[0]->title}}</p>
          </div>
          @if(count($articleSectionOne)>1)
          <div class="col-md-6">
            <div class="row clearfix">
              @foreach($articleSectionOne as $article)
              @continue($loop->first)
              <div class="custom-width col-sm-6 custom-padding">
                <div class="multi-content">
                  <a href="{{$article->article_type_id == 'post' ?route('frontend.post.show',\App\Libraries\Encryption::encodeId($article->id)) : route('frontend.video.show',\App\Libraries\Encryption::encodeId($article->id))}}"><img width="100%" height="100%" src="{{$article->image ? asset('uploads/article/image/'.$article->image) : asset('assets/frontend/img/case/1.jpg')}}" alt=""></a>
                  @if($article->article_type_id == 'video')
                  <a href="{{$article->article_type_id == 'post' ?route('frontend.post.show',\App\Libraries\Encryption::encodeId($article->id)) : route('frontend.video.show',\App\Libraries\Encryption::encodeId($article->id))}}" class="btn btn-video"><i class="fas fa-play"></i></a>
                  @endif
                </div>
                <p>{{$article->title}}</p>
              </div>
              @endforeach
            </div>
          </div>
          @endif
        </div>
      </div>
      @endif
      @if(count($articleSectionTwo)!=0)
      <div class="col-md-6 testimonial-box testimonial-items">
        <div class="row" style="padding-left:10px;">
          <div class="col-md-12 custom-padding">
            <div class="section-two-single-content">
              <a href="{{$articleSectionTwo[0]->article_type_id == 'post' ?route('frontend.post.show',\App\Libraries\Encryption::encodeId($articleSectionTwo[0]->id)) : route('frontend.video.show',\App\Libraries\Encryption::encodeId($articleSectionTwo[0]->id))}}"><img width="100%" height="100%" src="{{$articleSectionTwo[0]->image ? asset('uploads/article/image/'.$articleSectionTwo[0]->image) : asset('assets/frontend/img/case/1.jpg')}}" alt=""></a>
              @if($articleSectionTwo[0]->article_type_id == 'video')
              <a href="{{$articleSectionTwo[0]->article_type_id == 'post' ?route('frontend.post.show',\App\Libraries\Encryption::encodeId($articleSectionTwo[0]->id)) : route('frontend.video.show',\App\Libraries\Encryption::encodeId($articleSectionTwo[0]->id))}}" class="btn btn-video"><i class="fas fa-play"></i></a>
              @endif
            </div>
            <p>{{$articleSectionTwo[0]->title}}</p>
            </div>
          <div class="col-md-12">
            <div class="row clearfix">
              @foreach($articleSectionTwo as $article)
              @continue($loop->first)
              <div class="custom-width col-sm-6 custom-padding">
                <div class="section-two-multi-content multi-content">
                  <a href="{{$article->article_type_id == 'post' ?route('frontend.post.show',\App\Libraries\Encryption::encodeId($article->id)) : route('frontend.video.show',\App\Libraries\Encryption::encodeId($article->id))}}"><img width="100%" height="100%" src="{{$article->image ? asset('uploads/article/image/'.$article->image) : asset('assets/frontend/img/case/1.jpg')}}" alt=""></a>
                  @if($article->article_type_id == 'video')
                  <a href="{{$article->article_type_id == 'post' ?route('frontend.post.show',\App\Libraries\Encryption::encodeId($article->id)) : route('frontend.video.show',\App\Libraries\Encryption::encodeId($article->id))}}" class="btn btn-video"><i class="fas fa-play"></i></a>
                  @endif
                </div>
                <p>{{$article->title}}</p>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
      @endif
    </div>
  </div>
</div>
@endsection
