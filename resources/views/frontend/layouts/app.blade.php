<!DOCTYPE html>
<html lang="en">
<head>
    <!-- ========== Meta Tags ========== -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('meta')

    <!-- ========== Page Title ========== -->
    <title>Tiny Content System</title>

    {!! Html::style('assets/frontend/css/bootstrap.min.css') !!}
    {!! Html::style('assets/frontend/css/font-awesome.min.css') !!}
    {!! Html::style('assets/frontend/css/flaticon-set.css') !!}
    {!! Html::style('assets/frontend/css/magnific-popup.css') !!}
    {!! Html::style('assets/frontend/css/owl.carousel.min.css') !!}
    {!! Html::style('assets/frontend/css/owl.theme.default.min.css') !!}
    {!! Html::style('assets/frontend/css/animate.css') !!}
    {!! Html::style('assets/frontend/css/bootsnav.css') !!}
    {!! Html::style('assets/frontend/css/style.css') !!}
    {!! Html::style('assets/frontend/css/responsive.css') !!}
    {!! Html::style('assets/frontend/jssocials-1.4/jssocials.css') !!}
    {!! Html::style('assets/frontend/jssocials-1.4/jssocials-theme-flat.css') !!}
    @yield('header-css')
</head>

<body>
     @yield('content')

{!! Html::script('assets/frontend/js/jquery-1.12.4.min.js') !!}
{!! Html::script('assets/frontend/js/bootstrap.min.js') !!}
{!! Html::script('assets/frontend/js/equal-height.min.js') !!}
{!! Html::script('assets/frontend/js/jquery.appear.js') !!}
{!! Html::script('assets/frontend/js/jquery.easing.min.js') !!}
{!! Html::script('assets/frontend/js/jquery.magnific-popup.min.js') !!}
{!! Html::script('assets/frontend/js/modernizr.custom.13711.js') !!}
{!! Html::script('assets/frontend/js/owl.carousel.min.js') !!}
{!! Html::script('assets/frontend/js/wow.min.js') !!}
{!! Html::script('assets/frontend/js/progress-bar.min.js') !!}
{!! Html::script('assets/frontend/js/isotope.pkgd.min.js') !!}
{!! Html::script('assets/frontend/js/imagesloaded.pkgd.min.js') !!}
{!! Html::script('assets/frontend/js/count-to.js') !!}
{!! Html::script('assets/frontend/js/bootsnav.js') !!}
{!! Html::script('assets/frontend/') !!}
{!! Html::script('assets/frontend/js/main.js') !!}
{!! Html::script('assets/frontend/jssocials-1.4/jssocials.min.js') !!}
@yield('footer-script')

</body>
</html>
