<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\Encryption;

class ArticleController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showPost($Id)
    {
      $decodedId   = Encryption::decodeId($Id);
      if($decodedId == null)
      return view('frontend.404');
      $data['article']   = Article :: getArticleInfo($decodedId);
      return view('frontend.post-details',$data);
    }

    public function showVideo($Id)
    {
      $decodedId   = Encryption::decodeId($Id);
      if($decodedId == null)
      return view('frontend.404');
      $data['article']   = Article :: getArticleInfo($decodedId);
      return view('frontend.video-details',$data);
    }

}
