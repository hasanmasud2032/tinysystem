<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\Encryption;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['articleSectionOne']  = Article :: getArticles(1);
        $data['articleSectionTwo']  = Article :: getArticles(2);
        return view('frontend.index',$data);
    }

}
