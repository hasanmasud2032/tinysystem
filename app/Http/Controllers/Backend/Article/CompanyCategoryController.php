<?php

namespace App\Http\Controllers\Backend\Company;

use App\Models\CompanyCategory;
use App\DataTables\CompanyCategoryDataTable;
use App\Libraries\Encryption;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Validation\Rule;

class CompanyCategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(CompanyCategoryDataTable $dataTable)
     {
       return $dataTable->render("backend.company.company-category.index");
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view("backend.company.company-category.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
          'name'    => ['required',Rule::unique('company_categories')->where(function($query){
                         $query->where('is_archive',false);})
                       ],
          'status'  => 'required',
      ]);

      $category          = new CompanyCategory();
      $category->name    = $request->input('name');
      $category->status  = $request->input('status');
      $category->save();
      return redirect(route('backend.admin.corporate.company.company-categories.index'))->with('flash_success','Company category created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $Id
     * @return \Illuminate\Http\Response
     */
    public function show($Id)
    {
      $decodedId               = Encryption::decodeId($Id);
      $data['companyCategory'] = CompanyCategory::getCompanyCategoryInfo($decodedId);
      return view("backend.company.company-category.view",$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $Id
     * @return \Illuminate\Http\Response
     */
    public function edit($Id)
    {
      $decodedId                = Encryption::decodeId($Id);
      $data['companyCategory']  = CompanyCategory::find($decodedId);
      return view("backend.company.company-category.edit",$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $Id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$Id)
    {
      $decodedId   = Encryption::decodeId($Id);

      $this->validate($request, [
          'name'   => ['required',Rule::unique('company_categories')->ignore($decodedId)->where(function($query){
                        $query->where('is_archive',false);})
                      ],
          'status' => 'required',
      ]);

      $category           = CompanyCategory ::find($decodedId);
      $category->name     = $request->input('name');
      $category->status   = $request->input('status');
      $category->save();
      return redirect(route('backend.admin.corporate.company.company-categories.index'))->with('flash_success','Company category updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $Id
     * @return \Illuminate\Http\Response
     */
    public function destroy($Id)
    {
      if(request()->ajax())
      {
        $decodedId                = Encryption::decodeId($Id);
        $category              = CompanyCategory::find($decodedId);
        $category->is_archive  = 1;
        $category->deleted_by  = auth()->user()->id;
        $category->deleted_at  = Carbon::now();
        $category->save();
        return response()->json($category);
      }
    }

}
