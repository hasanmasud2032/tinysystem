<?php

namespace App\Http\Controllers\Backend\Article;

use App\Models\Article;
use App\DataTables\ArticleDataTable;
use App\Libraries\Encryption;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;

class ArticleController extends Controller
{
    protected $content;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function index(ArticleDataTable $dataTable)
     {
       return $dataTable->render("backend.article.index");
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view("backend.article.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
          'title'    => ['required',Rule::unique('articles')->where(function($query)use($request){
                         $query->where(['is_archive'=>false,'article_type_id'=>$request->get('article_type_id')]);})
                       ],
          'article_type_id'      => 'required|string',
          'section_id'           => 'required|string',
          'description'          => 'required|string',
          'image'                => 'required|mimes:jpeg,jpg,png',
          'link'                 => 'required_if:article_type_id,video',
          'status'               => 'required',
      ]);

        $Image = $this->StorePhoto($request->file('image'));
        if ($Image== false)
          return redirect()->back()->with('flash_warning', 'Image format is not valid. Only PNG or JPEG or JPG type images are allowed.')->withInput();

      $article  = new Article();
      $article->title                = $request->input('title');
      $article->article_type_id      = $request->input('article_type_id');
      $article->section_id           = $request->input('section_id');
      $article->description          = $request->input('description');
      $article->image                = $Image;
      if($request->has('link'))
      $article->video_link           = $request->input('link');
      $article->status               = $request->input('status');
      $article->save();
      return redirect(route('backend.admin.article.index'))->with('flash_success','Article created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $Id
     * @return \Illuminate\Http\Response
     */
    public function show($Id)
    {
      $decodedId        = Encryption::decodeId($Id);
      $data['article']  = Article::getArticleInfo($decodedId);
      return view("backend.article.view",$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $Id
     * @return \Illuminate\Http\Response
     */
    public function edit($Id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $Id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$Id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $Id
     * @return \Illuminate\Http\Response
     */
    public function destroy($Id)
    {
      if(request()->ajax())
      {
        $decodedId = Encryption::decodeId($Id);
        $article  = Article::find($decodedId);
        if($article->article_type_id == 'post')
        $this->DeleteExistImage($article,'/uploads/article/image/','content');
        $article->is_archive  = 1;
        $article->deleted_by  = auth()->user()->id;
        $article->deleted_at  = Carbon::now();
        $article->save();
        return response()->json($article);
      }
    }

    public static function DeleteExistImage($object,$path,$name)
    {
      if(file_exists(public_path().$path.$object->$name))
      {
        unlink(public_path().$path.$object->$name);
      }
    }

    // use store multi image at a time
    public static function StorePhoto($file){
          $directory = public_path('uploads/article/image/');
          if($file)
          {
            $fileName = trim(str_replace('.','',uniqid('Article'."_",true)).'.'.$file->getClientOriginalExtension());

            if(!file_exists($directory))
                mkdir($directory,0777,true);
            $mimeType = $file->getClientMimeType();
            if (!in_array($mimeType, ['image/jpg', 'image/jpeg', 'image/png']))
                    return false;
            $path = $directory.$fileName;
            Image::make($file->getRealPath())->resize(500,500)->save($path);
            return $fileName;
          }
          return false;
    }

}
