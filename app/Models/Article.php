<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
  use SoftDeletes;

  protected $table = 'articles';
  protected $fillable = [
      'id',
      'article_type_id',
      'section_id',
      'image',
      'video_link',
      'title',
      'description',
      'status',
      'is_archive',
      'created_by',
      'updated_by',
      'deleted_by',
      'deleted_at',
      'created_at',
      'updated_at',
  ];

  public static function getArticleList()
  {
      $query = Article::where('is_archive',false);
      return $query->orderBy('id','desc');
  }

  public static function getArticleInfo($Id)
  {
    return Article::join('users','users.id','=','articles.created_by')
    ->where('articles.id',$Id)
    ->select('articles.*','users.name as author_name')
    ->first();
  }

  public static function getArticles($sectionId)
  {
    return Article::where(['is_archive'=>false,'status'=>true,'section_id'=>$sectionId])
    ->select('articles.*')
    ->latest()
    ->get();
  }

  public static function boot()
  {
      parent::boot();
      static::creating(function($data){
        if(auth()->check())
        {
          $data->created_by = auth()->user()->id;
          $data->updated_by = auth()->user()->id;
        }
      });

      static::updating(function($data) {
          $data->updated_by = auth()->user()->id;
      });
  }

}
