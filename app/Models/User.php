<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'id',
         'name',
         'email',
         'phone',
         'password',
         'user_role_id',
         'image',
         'is_email_verified',
         'email_verification_token',
         'status',
         'is_archive',
         'created_by',
         'updated_by',
         'deleted_by',
         'deleted_at',
         'created_at',
         'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function getUserList($userId)
    {
      $query = User:: where('id',$userId);
      return $query->orderBy('id','desc');
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($data) {
          if(auth()->check())
          {
            $data->created_by = auth()->user()->id;
            $data->updated_by = auth()->user()->id;
          }
        });

        static::updating(function($data) {
            $data->updated_by = auth()->user()->id;
        });
    }

}
