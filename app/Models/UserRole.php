<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserRole extends Model
{
  use SoftDeletes;

  protected $table = 'user_roles';
  protected $fillable = [
      'id',
      'name',
      'status',
      'is_archive',
      'created_by',
      'updated_by',
      'deleted_by',
      'deleted_at',
      'created_at',
      'updated_at',
  ];

  public static function getUserRoleList()
  {
      $query = UserRole::where('is_archive',false);
      return $query->orderBy('id','desc');
  }

  public static function getUserRoleInfo($userRoleId)
  {
    return UserRole::join('users','users.id','=','user_roles.created_by')
    ->where('user_roles.id',$userRoleId)
    ->select('user_roles.*','users.name as author_name')
    ->first();
  }


  public static function boot()
  {
      parent::boot();
      static::creating(function($data){
        if(auth()->check())
        {
          $data->created_by = auth()->user()->id;
          $data->updated_by = auth()->user()->id;
        }
      });

      static::updating(function($data) {
          $data->updated_by = auth()->user()->id;
      });
  }

}
