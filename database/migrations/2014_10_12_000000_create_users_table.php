<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name',64);
          $table->string('email',64)->unique();
          $table->string('phone',20)->unique()->nullable();
          $table->string('password',100);
          $table->string('image')->nullable();
          $table->string('gender',20);
          $table->integer('user_role_id')->nullable();
          $table->tinyInteger('is_email_verified')->default(0);
          $table->string('email_verification_token',64);
          $table->timestamp('email_verified_at')->nullable();
          $table->tinyInteger('status')->default(1);
          $table->tinyInteger('is_archive')->default(0);
          $table->rememberToken();
          $table->integer('created_by')->nullable();
          $table->integer('updated_by')->nullable();
          $table->integer('deleted_by')->nullable();
          $table->softDeletes();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
