<?php

use Illuminate\Database\Seeder;
use App\Models\UserRole;

class UserRoleTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      UserRole::create([
          'name' => 'Admin',
          'created_by' => 1,
      ]);
    }
}
